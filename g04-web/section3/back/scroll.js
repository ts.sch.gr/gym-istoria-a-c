function clickIt()  
{
	window.status=window.event.srcElement.href;
}

function unloadIt()
{
	if (window.status.indexOf("/images/")>0)
		oPersistDiv.setAttribute("sPersistValue",document.all['divContent'].style.top);
	else
		oPersistDiv.setAttribute("sPersistValue","0px");
	oPersistDiv.save("oXMLStore");
}

function loadIt()
{
	oPersistDiv.load("oXMLStore");
	i=oPersistDiv.getAttribute("sPersistValue");
	i=i.substring(1,i.indexOf("px"));
	objScroller.down(i);
	return document.returnValue;
}

function verifyCompatibleBrowser(){ 
    this.ver=navigator.appVersion 
    this.dom=document.getElementById?1:0 
    this.ie5=(this.ver.indexOf("MSIE 5")>-1 && this.dom)?1:0; 
    this.ie4=(document.all && !this.dom)?1:0; 
    this.ns5=(this.dom && parseInt(this.ver) >= 5) ?1:0; 
    this.ns4=(document.layers && !this.dom)?1:0; 
    this.bw=(this.ie5 || this.ie4 || this.ns4 || this.ns5) 
    return this 
} 
bw=new verifyCompatibleBrowser() 
var speed=50 
var loop, timer 

function ConstructObject(obj,nest){ 
    nest=(!nest) ? 'document.Layer1.':'document.Layer1.document.'+nest+'.' 
	this.el=bw.dom?document.getElementById(obj):bw.ie4?document.all[obj]:bw.ns4?eval(nest+'document.'+obj):0; 
	this.css=bw.dom?document.getElementById(obj).style:bw.ie4?document.all[obj].style:bw.ns4?eval(nest+'document.'+obj):0; 
	this.scrollHeight=bw.ns4?this.css.document.height:this.el.offsetHeight 
	this.clipHeight=bw.ns4?this.css.clip.height:this.el.offsetHeight 
	this.up=MoveAreaUp;this.down=MoveAreaDown; 
	this.MoveArea=MoveArea; this.x; this.y; 
	this.obj = obj + "Object" 
	eval(this.obj + "=this") 
	return this 
} 

function MoveArea(x,y){ 
    this.x=x;this.y=y 
    this.css.left=this.x 
    this.css.top=this.y 
} 

function MoveAreaDown(move){ 
	if(this.y>-this.scrollHeight+objContainer.clipHeight){ 
    this.MoveArea(0,this.y-move) 
    if(loop) setTimeout(this.obj+".down("+move+")",speed) 
	} 
} 

function MoveAreaUp(move){ 
	if(this.y<0){ 
    this.MoveArea(0,this.y-move) 
    if(loop) setTimeout(this.obj+".up("+move+")",speed) 
	} 
} 

function PerformScroll(speed){ 
	if(initialised){ 
		loop=true; 
		if(speed>0) objScroller.down(speed) 
		else objScroller.up(speed) 
	} 
} 

function CeaseScroll(){ 
    loop=false 
    if(timer) clearTimeout(timer) 
} 

var initialised; 

function InitialiseScrollableArea(){ 
    objContainer=new ConstructObject('divContainer') 
    objScroller=new ConstructObject('divContent','divContainer') 
    objScroller.MoveArea(0,0) 
    objContainer.css.visibility='visible' 
    initialised=true; 
}

function displayStatusMsg(msgStr) { 
  status=msgStr;
  document.returnValue = true;
}


function findObj(n, d) { 
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=findObj(n,d.layers[i].document); return x;
}

function reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.pgW=innerWidth; document.pgH=innerHeight; onresize=reloadPage; }}
  else if (innerWidth!=document.pgW || innerHeight!=document.pgH) location.reload();
}
reloadPage(true);
